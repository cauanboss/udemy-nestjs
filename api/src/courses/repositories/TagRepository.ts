import { Repository } from 'typeorm';
import { Tag } from '../entities/tag.entity';

export class TagRepository extends Repository<Tag> {}
