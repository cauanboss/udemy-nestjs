import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // após colocar essa linha teremos que instalar
  // npm i class-validator class-transformer
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true, // tudo que não faz parte dos objetos de DTO, esse dado é retirado
      forbidNonWhitelisted: true,
      transform: true, // Transforma no tipo que esta no TDO
    }),
  );

  await app.listen(3000);
}
bootstrap();
